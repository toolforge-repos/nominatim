const express = require( "express" ),
    httpProxy = require('http-proxy'),
    HttpProxyRules = require('http-proxy-rules'),
    path = require('path'),
    url = require('url'),
    rateLimit = require('express-rate-limit'),
    RedisStore = require('rate-limit-redis');

const port = parseInt(process.env.PORT || 8080, 10) ; // IMPORTANT!! You HAVE to use this environment variable as port!
const proxy = httpProxy.createProxyServer({});

// convenience functions for filtering objects
function pick(obj, keys) {
    return Object.assign({}, ...keys.map(k => k in obj ? {[k]: obj[k]} : {}))
}

function omit(obj, keys) {
    return Object.assign({}, ...Object.keys(obj).filter(k => !keys.includes(k)).map(k => ({[k]: obj[k]})))
}

// Configure the proxy
const proxyRules = new HttpProxyRules({
    rules: {
        '/nominatim/search/([^/\?]+)': 'https://nominatim.openstreetmap.org/search/$1',
        '/nominatim/search': 'https://nominatim.openstreetmap.org/search',
        '/nominatim/reverse': 'https://nominatim.openstreetmap.org/reverse',
        '/nominatim/lookup': 'https://nominatim.openstreetmap.org/lookup',
        '/nominatim/details': 'https://nominatim.openstreetmap.org/details',
        '/search/([^/\?]+)': 'https://nominatim.openstreetmap.org/search/$1',
        '/search': 'https://nominatim.openstreetmap.org/search',
        '/reverse': 'https://nominatim.openstreetmap.org/reverse',
        '/lookup': 'https://nominatim.openstreetmap.org/lookup',
        '/details': 'https://nominatim.openstreetmap.org/details',
    },
    // default: 'https://nominatim.openstreetmap.org/search' // default target
});

proxy.on('proxyReq', function(proxyReq, req, res, options) {
    console.log('RAW Request from the target', JSON.stringify(req.headers, true, 2));
    req.headers = omit(req.headers, [ 'cookie', 'authentication', 'accept-encoding',
    'x-request-id', 'x-real-ip', 'x-forwarded-for', 'x-forwarded-host', 'x-forwarded-port',
    'x-forwarded-proto', 'x-original-uri', 'x-scheme' ]);
    console.log('Modified Request from the target', JSON.stringify(req.headers, true, 2));
});

proxy.on('proxyRes', function (proxyRes, req, res) {
    console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
    proxyRes.headers = pick(proxyRes.headers, [
        'date',
        'server',
        'content-type',
        'access-control-allow-methods',
        'vary',
        'transfer-encoding',
        'content-encoding'
    ]);
    proxyRes.headers['cache-control'] = 'public, max-age=3600';
    if( !req.headers.referer ) {
        console.error('No referrer');
        console.log('Changed Response from the target', JSON.stringify(proxyRes.headers, true, 2));
        return;
    }
    let parsedUrl = url.parse( req.headers.referer );
    if(
        parsedUrl.hostname.endsWith('toolforge.org') ||
        parsedUrl.hostname.endsWith('tools.wmflabs.org') ||
        parsedUrl.hostname.endsWith('wmcloud.org') ||
        parsedUrl.hostname.endsWith('wikilovesmonuments.org') ||
        parsedUrl.hostname.endsWith('localhost')
    ) {
        proxyRes.headers['access-control-allow-origin'] = parsedUrl.protocol + '//' + parsedUrl.host;
    }
    console.log('Changed Response from the target', JSON.stringify(proxyRes.headers, true, 2));
});

proxy.on('error', function (err, req, res) {
    console.error(err)
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end("We encountered a problem");
});

// We don't want people to load HTML which can load non wmflabs resources
function queryCheck(req, res, next){
    if(
        req.path === '/nominatim/search' ||
        req.path === '/nominatim/search/' ||
        req.path === '/nominatim/details' ||
        req.path === '/nominatim/details/' ||
        req.path === '/search' ||
        req.path === '/search/' ||
        req.path === '/details' ||
        req.path === '/details/'
    ){
	if(!req.query || !req.query.format || req.query.format === 'html') {
		next(new Error('Not allowed'));
	}
    }
    next();
}

// Request handler for nominatim proxy
function nominatimProxyHandler(req, res){
    var target = proxyRules.match(req);
    if( target ) {
        proxy.web(req, res, {
            target: target,
            changeOrigin: true,
            followRedirects: true,
            proxyTimeout: 2000,
            xfwd: true,
            headers: {
                'User-Agent': 'NominatimProxy/1.0 (CSP proxy for wmflabs: https://nominatim.toolforge.org/; nominatim@tools.wmflabs.org)',
                'X-User-Agent': 'NominatimProxy/1.0 (CSP proxy for wmflabs: https://nominatim.toolforge.org/; nominatim@tools.wmflabs.org)',
            }
        });
    } else {
        res.writeHead(404, {
            'Content-Type': 'text/plain'
        });
        res.end("Page Not Found");
    }
}

// Default config for response slowing
let rateLimitConfig = {
    windowMs: 1 * 60 * 1000, // 1 minute
    max: 60, // limit each IP to 60 requests per windowMs
    message: 'Too many requests in quick succession. ' +
        'All users of this API share a 1 req/second request rate. ' +
        'Please debounce your requests or have some patience.',
    handler: function (req, res, next, options) {
        if (req.rateLimit.used === req.rateLimit.limit + 1) {
            console.log((new Date()).toISOString() + ' - Rate limit triggered.');
        }
        res.status(options.statusCode).send(options.message)
    },
}

// Use redis when we are on tools
if(process.env.HOME === '/data/project/nominatim/') {
    let redis = require('redis');
    rateLimitConfig.store = new RedisStore({
        expiry: 60 * 60, // an hour
        prefix: 'nominatim_rl:',
        client: redis.createClient({
            host: 'tools-redis'
        })
    })
}
// create the limiter middleware
const limiter = rateLimit(rateLimitConfig);

// Configure and setup webserver
const app = express();

// We usually run behind the tools proxy
app.set('trust proxy', true);

app.get([
    '(/nominatim|)/search/:query',
    '(/nominatim|)/search',
    '(/nominatim|)/reverse',
    '(/nominatim|)/lookup',
    '(/nominatim|)/details',
], queryCheck, limiter, nominatimProxyHandler);

// page with help
app.get('(/nominatim|)/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});
app.get('(/nominatim|)/toolinfo.json', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'toolinfo.json'));
});

/* catch all
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});
*/

app.listen(port, () => console.log(`Nominatim proxy is listening on port ${port}!`))
